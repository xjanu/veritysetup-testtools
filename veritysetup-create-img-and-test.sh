#!/bin/bash

function corrupt_device() # $1 device, $2 device_size(in bytes), $3 #{corrupted_bytes}
{
	# Repeatable magic corruption :-)
	CORRUPT=$3
	RANDOM=43
	while [ "$CORRUPT" -gt 0 ]; do
		SEEK=$RANDOM
		while [ $SEEK -ge $2 ] ; do SEEK=$RANDOM; done
		echo -n -e "\x55" | dd of=$1 bs=1 count=1 seek=$SEEK conv=notrunc > /dev/null 2>&1
		CORRUPT=$(($CORRUPT - 1))
	done
}

# $1 data_device, $2 hash_device, $3 fec_device, $4 data/hash_block_size(in bytes),
# $5 data_size(in blocks), $6 device_size(in blocks), $7 hash_offset(in bytes),
# $8 fec_offset(in bytes), $9 fec_roots, ${10} corrupted_bytes, [${11} superblock(y/n), ${12} salt]
function check_fec()
{
	INDEX=25
	dd if=/dev/zero of=$1 bs=$4 count=$6 > /dev/null 2>&1

	echo -n "Block_size: $4, Data_size: $(($4 * $5))B, FEC_roots: $9, Corrupted_bytes: ${10} "

	PARAMS=" --data-block-size=$4 --hash-block-size=$4 "
	if [ "$5" -ne "$6" ]; then
		PARAMS="$PARAMS --data-blocks=$5"
	fi

	if [ "$7" -ne 0 ]; then
		PARAMS="$PARAMS --hash-offset=$7"
	fi

	if [ "$8" -ne 0 ]; then
		PARAMS="$PARAMS --fec-offset=$8"
	fi

	if [ "${11}" == "n" ]; then
		INDEX=24
		echo -n "[no-superblock]"
		PARAMS="$PARAMS --no-superblock -s=${12}"
	elif [ -n "${12}" ]; then
		PARAMS="$PARAMS -s=${12}"
	fi

	if [[ "$1" == "$2" && "$1" == "$3" ]]; then
		echo -n "[one_device_test]"
		dd if=/dev/zero of=$IMG_TMP bs=$4 count=$5  > /dev/null 2>&1
		ARR=(`sha256sum $IMG_TMP`)
		HASH_ORIG=${ARR[0]}
	else
		ARR=(`sha256sum $1`)
		HASH_ORIG=${ARR[0]}
	fi

	ARR=(`$VERITYSETUP format $1 $2 --fec-device=$3 $PARAMS`)
	SALT=${ARR[$INDEX]}
	ROOT_HASH=${ARR[$(($INDEX+3))]}

	echo "root hash: $ROOT_HASH"

	corrupt_device $1 $(($5 * $4)) ${10}

    echo
    echo "Userspace test"
    #libtool --mode=execute gdb --args ...
	$VERITYSETUP verify -v $1 $2 $ROOT_HASH --fec-device=$3 $PARAMS
    #echo "Kernel test"
	#$VERITYSETUP create -v $DEV_NAME $1 $2 $ROOT_HASH --fec-device=$3 $PARAMS # > /dev/null 2>&1
    return 3
	if [ "$?" -ne "0" ] ; then
		echo "[N/A, test skipped]"
		return 3
	fi

	udevadm settle

	dd if=/dev/mapper/$DEV_NAME of=$IMG_TMP > /dev/null 2>&1
	ARR=(`sha256sum $IMG_TMP`)

	HASH_REPAIRED=${ARR[0]}

	#$VERITYSETUP close $DEV_NAME
	#rm $1 $2 $3 $IMG_TMP > /dev/null 2>&1

	if [ "$HASH_ORIG" != "$HASH_REPAIRED" ]; then
		echo "[correction failed]"
		return 1
	fi

	echo "[file was repaired][OK]"
}

VERITYSETUP=../veritysetup
DEV_NAME=devtest
IMG_TMP=temp.img

data_size=$1
device_size=$(($data_size*3))
hash_offset=$(($data_size*512))
fec_offset=$(($hash_offset*2))

echo "Data size:   $data_size"
echo "Device size: $device_size"
echo "Hash offset: $hash_offset"
echo "FEC offset:  $fec_offset"

rm *.img *.hex

echo
echo "-- ONE DEVICE --"
check_fec onedevice.img onedevice.img onedevice.img 512 $data_size \
          $device_size $hash_offset $fec_offset 2 1 n \
          9e7457222290f1bac0d42ad2de2d602a87bb871c22ab70ca040bad450578a436
echo
echo "-- SEPARATE FEC DEVICE --"
check_fec sepdevice.img sepdevice.img fecdevice.img 512 $data_size \
          $device_size $hash_offset 0 2 1 n \
          9e7457222290f1bac0d42ad2de2d602a87bb871c22ab70ca040bad450578a436

echo
echo "-- COMPARE FEC DATA --"
dd if=onedevice.img of=onedevice-fec.img bs=1 skip=$fec_offset

xxd onedevice-fec.img onedevice-fec.hex
xxd fecdevice.img     fecdevice.hex

diff -s --unchanged-line-format= *.hex | head -n 1 | cut -f 1 --delimiter=':'
