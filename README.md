Veritysetup test tools
======================

Do not use code from this repo for anything more than some crude tests.
You have been warned.

This repo is intended to be in a subdirectory of the
[cryptsetup/cryptsetup](https://gitlab.com/cryptsetup/cryptsetup) project.

------
## veritysetup-create-img-and-test.sh
This tool creates a set of disk images by using veritysetup with fec, introduces
an error into the images, tests whether the errors can be corrected, and
compares fec data when using the same, vs. when using a separate device for it.

### Usage:
`veritysetup-create-img-and-test.sh _param_`  
where _param_ is the size of data region in 512B blocks.

------
## main.c
This tool is supposed to bypass veritysetup program and test a device for errors
by direcly using libcryptsetup functions.

### Configure:
Make sure to change the `#define`s in the source code according to your needs.

### Compile:
`gcc -o test -g -lcryptsetup -I../lib/ main.c ../lib/utils_crypt.c`

### Run:
Change value of `LD_PRELOAD` environment variable to include the
`libcryptsetup.so` that you want, and simply run the executable `./test`.
