#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "libcryptsetup.h"
#include "utils_crypt.h"

#define SALT "9e7457222290f1bac0d42ad2de2d602a87bb871c22ab70ca040bad450578a436"
#define HASH "ee79929c132bd6fec4581c8cb0039e74950836426b6c0098f924c7399797f3f3"
#define NAME "onedevice.img"
#define BLOCK_SIZE 512
#define DATA_SIZE 4
#define FEC_OFFSET 4096
#define FEC_ROOTS 2
#define HASH_OFFSET 2048


int main()
{
    char root_hash[] = HASH;
    struct crypt_params_verity vp;
    vp.data_block_size = BLOCK_SIZE;
    vp.data_device = NAME;
    vp.data_size = DATA_SIZE;
    vp.fec_area_offset = FEC_OFFSET;
    vp.fec_device = NAME;
    vp.fec_roots = FEC_ROOTS;
    vp.flags = CRYPT_VERITY_NO_HEADER | CRYPT_VERITY_CHECK_HASH;
    vp.hash_area_offset = HASH_OFFSET;
    vp.hash_block_size = BLOCK_SIZE;
    vp.hash_device = NAME;
    vp.hash_name = "sha256";
    vp.hash_type = 1;
    int ret;

    struct crypt_device *cd = NULL;
    ret = crypt_init_data_device(&cd, vp.hash_device, vp.data_device);
    if (ret != 0) {
        fprintf(stderr, "Could not init device\n%s\n", strerror(ret));
        crypt_free(cd);
        return 1;
    }

    char *salt;
    vp.salt_size = crypt_hex_to_bytes(SALT, &salt, 0);
	if (vp.salt_size < 0) {
		fprintf(stderr, "Invalid salt string specified.");
        crypt_free(cd);
        return 1;
    }
    vp.salt = salt;

    ret = crypt_format(cd, CRYPT_VERITY, NULL, NULL, NULL, NULL, 0, &vp);
    if (ret != 0) {
        fprintf(stderr, "Could not format device\n");
        crypt_free(cd);
        return 1;
    }

    char *root_hash_bytes;
	int hash_size = crypt_get_volume_key_size(cd);
	if (crypt_hex_to_bytes(root_hash, &root_hash_bytes, 0) != hash_size) {
		fprintf(stderr, "Invalid root hash string specified.");
        return 1;
	}

    ret = crypt_activate_by_signed_key(cd, NULL,
					 root_hash_bytes,
					 hash_size,
					 NULL, 0,
					 CRYPT_ACTIVATE_READONLY);

    crypt_free(cd);
    return 0;
}
